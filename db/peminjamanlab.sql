-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 03 Jul 2020 pada 00.41
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `peminjamanlab`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `barang`
--

CREATE TABLE `barang` (
  `IdBrg` int(3) NOT NULL,
  `NamaBrg` varchar(100) DEFAULT NULL,
  `tahun` varchar(4) DEFAULT NULL,
  `lab` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `barang`
--

INSERT INTO `barang` (`IdBrg`, `NamaBrg`, `tahun`, `lab`) VALUES
(1, 'Proyektor', '2008', 'Jarkom'),
(2, 'Router', '2008', 'Jarkom'),
(3, 'LAN Tester', '2008', 'Jarkom'),
(4, 'Breadboard', '2009', 'TETD'),
(5, 'Adruino', '2010', 'TETD'),
(6, 'Multimeter', '2011', 'TETD'),
(7, 'Jangkar Sorong', '2012', 'Fisika'),
(8, 'Mikro Meter', '2013', 'Fisika'),
(9, 'Pen Tablet', '2015', 'DKV'),
(10, 'Kamera', '2014', 'DKV'),
(11, 'Mesin Bubut', '2018', 'Industri'),
(12, 'Mesin Las', '2018', 'Industri'),
(13, 'Monitor', '2015', 'Programing'),
(14, 'Layar', '2018', 'Programing'),
(15, 'Buku', '2020', 'Jarkom');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `nim` int(8) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `NamaBrg` varchar(100) NOT NULL,
  `kepentingan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`nim`, `nama`, `NamaBrg`, `kepentingan`) VALUES
(18102142, 'Jodi', 'Monitor', 'UKM'),
(18102143, 'veza', 'Mesin Las', 'pribadi'),
(18102144, 'Syahrul', 'Lan Tester', 'Pribadi');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`IdBrg`),
  ADD KEY `NamaBrg` (`NamaBrg`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`nim`),
  ADD UNIQUE KEY `NamaBrg_2` (`NamaBrg`),
  ADD KEY `NamaBrg` (`NamaBrg`);

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD CONSTRAINT `peminjaman_ibfk_1` FOREIGN KEY (`NamaBrg`) REFERENCES `barang` (`NamaBrg`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
